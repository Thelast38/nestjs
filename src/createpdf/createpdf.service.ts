import { Injectable, Res } from '@nestjs/common';
import {Response} from 'express';
import * as ejs from "ejs";
import * as pdfMake from "html-pdf";
import * as mysql from "mysql";

@Injectable()
export class CreatePdfService {
  getDate(): string {
    var date = new Date(); 
    var day = date.getDate();
    var strDay : string;
    var strMonth : string;
    var strHour : string;
    var strMin : string;
    if (day < 10){
    strDay = String(day);
    strDay = "0" + strDay;
    } else {
    strDay = String(day);
    }
    var month = date.getMonth() + 1;
    if (month < 10){
    strMonth = String(month);
    strMonth = "0" + month;
    } else{
    strMonth = String(month);
    }
    const year = date.getFullYear();
    var hour = date.getHours();
    if (hour < 10){
    strHour = String(hour);
    strHour = "0" + hour;
    } else {
    strHour = String(hour);
    }
    var min = date.getMinutes();
    if (min < 10){
    strMin = String(min);
    strMin = "0" + min;
    } else {
    strMin = String(min);
    }
    var allDate = strHour + ":" + strMin + " " + strDay + "." + strMonth + "." + String(year);
    return allDate;
  }
  createPdf(params, fileName: string, pathToEjs:string, @Res() res: Response): void{
    ejs.renderFile(__dirname + pathToEjs, params, function(err, html) {
        if(err){
            console.log(err);
        }
       
        const options = { format: 'A4', orientation: 'portrait'};
          pdfMake.create(html, options).toBuffer(function(err, buffer){
            if (err){
              res.end("Error");
              console.log(err);
            }
           
    
            res.writeHead(200, {
              "Content-type": "application/pdf",
              "Content-Disposition": 'attachment;filename="{name}.pdf"'.replace('{name}', fileName)
            });
           return res.end(buffer);
    });
    });
  }

  getRandomInt(min: number, max: number): number{
    return Math.floor(Math.random() * (max - min)) + min;
  }
  getParamsFromSql(id, date, par){
    let params;
    let str:string;
    for (var i = 0; i < id.id.length;i++ ){
      if(i == 0){
        str = id.id[i];
      } else {
        str += "," + id.id[i];
      }
      
    }
    let sql = mysql.createConnection({
      host: "localhost",
      user: "anton",
      password: "1234",
      database: "jiskefet"
    });
    sql.connect(function(err) {
      if (err) {
        console.log(err);
      }
      console.log("Connected!");
      sql.query(`select run_number, time_o2_start, time_trg_start, time_o2_end, time_trg_end from run where run_number in(${str})`, function (err, result) {
        if (err) {
          console.log(err);
        }
        let run_id:string[] = new Array(result.length);
        let time_o2_start:string[] = new Array(result.length);
        let time_trg_start:string[] = new Array(result.length);
        let time_o2_end:string[] = new Array(result.length);
        let time_trg_end:string[] = new Array(result.length);
        for (var i = 0; i < result.length; i ++){
          run_id[i] = result[i].run_number;
          time_o2_start[i] = result[i].time_o2_start;
          time_trg_start[i] = result[i].time_trg_start;
          time_o2_end[i] = result[i].time_o2_end;
          time_trg_end[i] = result[i].time_trg_end;
        }

       params = {run_id : run_id, time_o2_start: time_o2_start, time_o2_end: time_o2_end, time_trg_start:time_trg_start, time_trg_end:time_trg_end, date: date};
       par = params;
       console.log(params);
       return params;

      })});
  }
  
}